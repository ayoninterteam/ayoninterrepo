<?php
 $page_id=2;
 include('includes/header.php'); ?>
<!--ends header-->
<div class="container-fluid" id="contact_banner">
	<img src="images/skip_waste.jpg" />
</div>
<!--Contact Section-->
<div id="contact" class="container">
  <div class="row">
      <div class="col-lg-12 about_head">
      	<h1>About us</h1>
      	<!--<p>Lorem ipsum dolor sit amet, consectettur adipiscing elit</p>-->
      </div>	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Ayon International. W.L.L</h3>
                        <p>Established in 2008 Ayon International is today one of the Middle East's popular and more respected company. Our fleet offers an unparalleled range of services to our customers.<br/><br/>
The company's line of business includes the distribution of water, waste water management, rental services of luxurious cars and lot more.<br/><br/>
We collaborate with our clients to  understand their specific needs in details. We are committed to deliver high quality of plants, improved technology  with devoted services and our un-beaten competitiveness ensures complete customer satisfaction.</p>
                    </div>
                </div> 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_right">
                        <img src="images/about_inner.jpg"/>
                    </div>	
                </div>
            </div>
        </div> 
           
    </div>
</div>
<div class="container-fluid vision_mision">
<div class="container">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_mission">
            <h3>Our Mission</h3>
            <p>Our people are committed to our customers needs by supplying the best advice and quality products backed by a high standard of service and are continually being trained and updated with new product knowledge and new technologies.</p>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_mission">
            <h3>Our Vision</h3>
            <p>Knowledge and experience, resources and infrastructure, advances in technology and environmental sustainability combine to equip Ayon as a market leader .</p>
        </div>
    </div>
</div>    
</div>
<?php include('includes/footer.php'); ?>