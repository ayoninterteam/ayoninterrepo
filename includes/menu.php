        <span class="nav_trigger"><i class="fa fa-bars"></i></span> 
        <div class="main_menu">
            <div class="container">
                <nav class="main_nav">
                    <ul>
                        <li><a <?php if($page_id==1){ echo 'class="active"'; } ?> href="index.php">Home</a></li>
                        <li><a href="#">About us</a></li>
                        <li><a <?php if($page_id==3||$page_id==3.1||$page_id==3.2){ echo 'class="active"'; } ?> href="booking.php">Booking</a></li>
                        <li><a href="#">Facilities</a></li>
                        <li><a <?php if($page_id==5){ echo 'class="active"'; } ?> href="package.php">Packages</a></li>
                        <li><a href="#">Gallery</a></li>
                        <li><a href="#">Restaurant</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                </nav>
            </div>
        </div>