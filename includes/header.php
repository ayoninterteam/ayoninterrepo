<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ayon</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/responsive.css" rel="stylesheet">
    <!--<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">-->
    <!--slider-->
    <?php if($page_id==1){ ?>
    <link href="css/skdslider.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="client-slider/initcarousel-1.css">
    <?php } ?>
    <?php if($page_id==4){ ?>
    <link rel="stylesheet" href="css/bootstrap.vertical-tabs.css">
    <?php } ?>
    <?php if($page_id==5){ ?>
    <link rel="stylesheet" href="index_files/vlb_files1/vlightbox1.css" type="text/css" />
	<link rel="stylesheet" href="index_files/vlb_files1/visuallightbox.css" type="text/css" media="screen" />
    <?php } ?>
</head>
<body>
<!--Header Section-->
    <header>
        <nav class="header_bar">
                    <div class="logo">
                        <a class="navbar-brand" style="padding:0" href="index.php"><img src="images/logo.png" alt="logo"></a>
                    </div>
                <span class="nav_trigger"><i class="fa fa-bars"></i></span> 
                <div class="main_menu" id="myNavbar">
                    <ul>
                        <li <?php if($page_id==1){ echo 'class="asd active"'; } else { echo 'class="asd"'; } ?>><a href="index.php">Home</a></li>
                        <li <?php if($page_id==2){ echo 'class="asd active"'; } else { echo 'class="asd"'; } ?>><a href="about.php">About Us</a></li>
                        <li <?php if($page_id==3){ echo 'class="asd active"'; } else { echo 'class="asd"'; } ?>><a href="service.php">Services</a></li>
                        <li <?php if($page_id==4){ echo 'class="asd active"'; } else { echo 'class="asd"'; } ?>><a href="career.php">Career</a></li>
                        <li <?php if($page_id==5){ echo 'class="asd active"'; } else { echo 'class="asd"'; } ?>><a href="gallery.php">Gallery</a></li>
                        <li <?php if($page_id==6){ echo 'class="asd active"'; } else { echo 'class="asd"'; } ?>><a href="contact.php">Contact US</a></li>
                    </ul>
                </div>
        </nav>
    </header>