<footer>
    <div class="container">
        <div class="row foot-row">
            <!--1st Col starts here-->
            <div class="col-lg-4 col-md-4 col-sm-3 col-bord">
                <div class="tab_2">
                    <div class="bw">
                        <a href="index.php"><img src="images/footer_logo.jpg"></a>
                    </div>
                </div>
            </div>
            <!--1st Col close here-->
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="tab_2">
                    <div class="quick">
                        <p class="con_head">important links</p>
                    </div>
                    <div class="foot_nav">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="career.php">Career</a></li>
                            <li><a href="service.php">Our Services</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>   
            <!--2nd Col starts here-->
            
            <div class="col-lg-4 col-md-4 col-sm-5 col-bord">
                <div class="tab_2">
                    <div class="addrs">
                        <p class="con_head">Quick CONTACT</p>
                        <p style="line-height:32px"><img src="images/locatin.png">
                            Nasser Bin Jabor Bin Al-Ahmed Al-Thani<br>
                            &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Doha - Qatar, P.O. Box : 13240
                            <br><img src="images/mail.png">info@ayoninternational.com
                            <br><img src="images/call.png">+974 44601417 &nbsp;<img src="images/whatapp.png">+974 66926726
                            <br><img src="images/fax.png">+974 44601417<br></p><!--
                            <a href=""><img src="images/fb.png"></a> <a href=""><img src="images/tw.png"></a> <a href=""><img src="images/google_plus.png"></a> -->
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--Copyrigth bar starts here-->

    <div class="container-fluid footer_bottom">
      <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="foot_bar">
                    <div class="copy"> © 2016 Ayon International WLL , All rights reserved, Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi.png"></a> </div>                  
                </div>
            </div>
        </div>
      </div>
    </div>
    <!--Copyrigth bar close here-->

</footer>
    
	<script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <?php if($page_id==1){ ?>
    <script src="js/skdslider.min.js"></script>
    <!--banner-slider-->
    <script type="text/javascript">
            jQuery(document).ready(function(){
                jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':false,'autoSlide':true,'animationType':'fading'});
                jQuery('#responsive').change(function(){
                  $('#responsive_wrapper').width(jQuery(this).val());
                });
                
            });
    </script>
    <!--ends banner-slider-->
<!--    <script src="client-slider/jquery.js"></script>
-->    <script src="client-slider/amazingcarousel.js"></script>    
	<script src="client-slider/initcarousel-1.js"></script>
    <?php } ?>
    <?php if($page_id==5){ ?>
	<script src="index_files/vlb_engine/visuallightbox.js" type="text/javascript"></script>
	<script src="index_files/vlb_engine/thumbscript1.js" type="text/javascript"></script>
	<script src="index_files/vlb_engine/vlbdata1.js" type="text/javascript"></script>
    <?php } ?>
</body>
</html>
