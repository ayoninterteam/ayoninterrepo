<?php
 $page_id=4;
 include('includes/header.php'); 
 require("Admin/config/config.inc.php"); 
require("Admin/config/Database.class.php");
require("Admin/config/Application.class.php");
 $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
 ?>
<!--ends header-->
<div class="container-fluid" id="contact_banner">
	<img src="images/career_banner.jpg" />
</div>
<!--Contact Section-->
<div id="career" class="container">
  <div class="row">
      <div class="col-lg-12 about_head">
      	<h1>A Career with us</h1>
      	<!--<p>Lorem ipsum dolor sit amet, consectettur adipiscing elit</p>-->
      </div>	
		<div class="col-lg-12 career_main_inner">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php
		$selectAll = "select  * from ".TABLE_CAREER;
		$result = $db->query($selectAll);
		$number	=	mysql_num_rows($result);
		if(mysql_num_rows($result)==0)
		{
		?>
			<p class="col-lg-5" style="float:none; margin:0 auto; "><img style="width:100%;padding-bottom:60px" src="images/no-vacancies.gif" /></p>
		<?php
		}
		else
		{	
			$i=0;
			while ($row = mysql_fetch_array($result)) 
				{	
				
		?>
        <!--<p class="col-lg-5" style="float:none; margin:0 auto; "><img style="width:100%;padding-bottom:60px" src="images/no-vacancies.gif" /></p>-->
            
                <div class="col-xs-12 career_items">
                    <div class="col-lg-12 career_head">
                    	<h1 style="margin-top:10px"><?= $row['heading']; ?></h1>
                    </div><!--End career_head-->
                    <div class="col-lg-12" style="margin-top:10px; padding:0">
                    	<!--<div class="col-lg-12 career_place"> Date : <span>25 - Oct - 2016</span></div>-->
                    	<div class="career_images_inner"></div>
                        <div class="container" style="margin-top:20px;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <!-- required for floating -->
                                    <!-- Nav tabs -->
                                        <ul class="nav nav-tabs tabs-left">
                                            <li class="active"><a href="#Tab-a<?= $i; ?>" data-toggle="tab">Job Description</a></li>
                                            <li><a href="#Tab-b<?= $i; ?>" data-toggle="tab">Responsibilities</a></li>
                                            <!--<li><a href="#Tab-c1" data-toggle="tab">Candidate Profile</a></li>-->
                                        </ul>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="Tab-a<?= $i; ?>">
                                            	<b style="color:#000">Position :</b> <?= $row['position']; ?><br/>
												<b style="color:#000">Vacancies :</b> <?= $row['vacancies']; ?><br/>
												<b style="color:#000">Location :</b> <?= $row['location']; ?><br/>
												<b style="color:#000">Experience :</b> <?= $row['experience']; ?><br/>
												<b style="color:#000">Qualification :</b> <?= $row['qualification']; ?></div>
                                            <div class="tab-pane" id="Tab-b<?= $i; ?>"><?= $row['responsibilities']; ?></div>
<!--                                            <div class="tab-pane" id="Tab-c1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
-->                                        </div>
                                    </div>
                                </div>
                            </div><!--End row-->
                        </div><!--end container inner-->
                    </div>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#bs-modal-a<?= $i; ?>" style="float:right;">Apply Now !</button>
                </div><!--End career_items-->
                
                
                <div class="modal fade" id="bs-modal-a<?= $i; ?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Apply Now</h4>
                        </div>
                        <div class="modal-body" style="height: auto !important;overflow: auto;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Enqry" style=" height:auto;margin-top:15px; padding:0;">
                                <form enctype="multipart/form-data" method="post" action="job-mail.php">
                                	<label>Position</label>
                                	<input type="text" name="position" value="<?= $row['position']; ?>" required="">
                                    <input type="text" name="name" placeholder="Full Name *" required="">
                                    <input type="email" name="mail" placeholder="Email *" required="">
                                    <input type="number" name="phone" placeholder="Contact Number *" required="">
                                    <textarea type="text" name="messege" placeholder="Messege *"></textarea>
                                    <label>Upload Resume</label>
                                    <input type="file" name="resume" style="padding-top:8px !important;" required="">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </form>
                            </div>        
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            
            
                <?php
				    $i++;
					}
				 }
                ?>
            </div>
            <!-- /.modal -->
            <div class="col-xs-12 career_items" style="margin-bottom:20px">
            </div>
            </div>
            
        </div>    
  </div>
</div>
<?php include('includes/footer.php'); ?>