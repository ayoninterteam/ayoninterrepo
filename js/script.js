/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function () {
    'use strict';
    $('.nav_trigger').click(function () {
        $('.main_menu').slideToggle();
    });
    $(".main_menu li a").click(function () {
        $(this).parent().addClass('active').siblings().removeClass('active');
    });
});
/*$(document).ready(function () {
    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        $('.main_menu a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('.main_menu li a').removeClass("active");
                currLink.addClass("active");
            }
            else {
                currLink.removeClass("active");
            }
        });
    }
});*/