<?php
 $page_id=3;
 include('includes/header.php'); ?>
<!--ends header-->
<div class="container-fluid" id="contact_banner">
	<img src="images/service_banner.jpg" />
</div>
<!--Contact Section-->
<div id="contact" class="container">
  <div class="row">
      <div class="col-lg-12 about_head">
      	<h1>Our Services</h1>
      	<!--<p>Lorem ipsum dolor sit amet, consectettur adipiscing elit</p>-->
      </div>	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main" id="service1">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Limousine Service and Rental Cars</h3>
                        <p>We guarantee absolute sublime, reliability and professionalism providing an exemplary standard of car rental service to 
                        companies and private individuals. Our aim is to achieve the highest possible standard. We lead the way in the relentless pursuit 
                        for excellence.We organize and carries out transportation services timely and accurate, providing our clients with an 
                        unparalleled driving experience; because that's what we're here to do - to out-deliver expectations on every significant 
                        criterion.</p>
                    </div>
                </div> 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_inner_right">
                        <img src="images/limousine.jpg"/>
                    </div>	
                </div>
            </div>
        </div> 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main" id="service2">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_inner_right">
                        <img src="images/sewage.jpg"/>
                    </div>	
                </div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Sewage Water Transportation</h3>
                        <p>We collect sewage from Camps, Work sites etc. and dispose it by following a well procedure. We have highly featured 
                        equipment to collect and dispose it in a good way. </p>
                    </div>
                </div> 
                
            </div>
        </div> 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main" id="service3">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
            	
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Supply of Drinking Water</h3>
                        <p>We cater our products to all major sectors in Qatar. Huge Corporates, Hotels, Offices and Residences across Qatar are our 
                        major consumers. The success in this competitive market is its quality, service and range of products based on 
                        customer’s requirements, backed by prompt, quick and friendly delivery service throughout Qatar. Thus thousands of houses, hotels 
                        and offices enjoying Bottled drinking water every day. Trust us and we take care of your satisfaction.</p>
                    </div>
                </div> 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_inner_right">
                        <img src="images/water_supply.jpg"/>
                    </div>	
                </div>
                
            </div>
        </div> 
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main" id="service4">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_inner_right">
                        <img src="images/skip_waste_remove.jpg"/>
                    </div>	
                </div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Skip Waste Removal  Services</h3>
                        <p>Effective waste disposal requires our sophisticated engineering and technical expertise to manage the waste in an 
                        environmentally responsible way.</p>
                    </div>
                </div> 
                
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main" id="service5">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Tyre & Battery</h3>
                        <p>We are proud to have built a strong reputation of providing quick and efficient service, as well as high-quality products 
                        at reasonable prices. We offer an impressive suite of tyres from some of today’s leading brands and  helps  you find the 
                        tyres & battery that best suits your individual requirement.
                        </p>
                    </div>
                </div> 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_inner_right">
                        <img src="images/tyre.jpg"/>
                    </div>	
                </div>
                
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main"  id="service6">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_inner_right">
                        <img src="images/cleaning.jpg"/>
                    </div>	
                </div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Cleaning & Maintenance</h3>
                        <p>Our reputation has been built over many years of successfully delivering a wide range of services under one umbrella. 
                        Customer satisfaction, quality service and value for money are our key focus.We provide a cleaning service tailored to the needs 
                        of our customers, within budget, on time, and with a dedication to getting it right every time.</p>
                    </div>
                </div> 
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner_main"  id="service7">
        	<div class="col-lg-12 col-md-12 col-sm-12" style="padding:0">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about_inner">
                    	<h3>Structure Fabrication</h3>
                        <p>Our company is recognized as the foremost service provider in Doha for Structural Steel Fabrication work that includes 
                        Classifier Body Fabrication, Structural Fabrication Job Work and Heavy Fabrication Job Work. Our appointed team uses high 
                        grade steel in fabricating the structures for various process industries. Our clients can avail these services from us at 
                        feasible rates
                        </p>
                    </div>
                </div> 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_inner_right">
                        <img src="images/structure.jpg"/>
                    </div>	
                </div>
                
            </div>
        </div>
        
           
    </div>
</div>

<?php include('includes/footer.php'); ?>