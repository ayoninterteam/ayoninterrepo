<?php
 $page_id=1;
 include('includes/header.php'); ?>
<!--ends header-->
<!--Banner Section-->
<ul id="demo1">
    <li><img src="images/banner1.jpg" /></li>
    <li><img src="images/banner2.jpg" /></li>
    <li><img src="images/banner3.jpg" /></li>
    <li><img src="images/banner5.jpg" /></li>
    <li><img src="images/banner4.jpg" /></li>
<!--    <li><img src="images/banner5.jpg" /></li>
    <li><img src="images/banner6.jpg" /></li>-->
    <div class="fixd_service">
    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fixd_service_head">
        	Services
        </div>
        <a href="service.php#service3">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fixd_service_item1">
        	<img src="images/water_supply_icon.png"><br>
            Supply of drinking water
        </div>
        </a>
        <a href="service.php#service4">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fixd_service_item2">
        	<img src="images/skip_waste_icon.png"><br>
            Skip waste removal services
        </div>
        </a>
        <a href="service.php#service6">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fixd_service_item1">
        	<img src="images/cleaning_icon.png"><br>
            Cleaning service & maintenance
        </div>
        </a>
        <a href="service.php#service1">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fixd_service_item2">
        	<img src="images/car_icon.png"><br>
            Limousine services and rental cars
        </div>
        </a>
        <a href="service.php#service5">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fixd_service_item1">
        	<img src="images/tyre_icon.png"><br>
            Tyre & battery
        </div>
        </a>
        <a href="service.php#service2">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fixd_service_item2">
        	<img src="images/sewage_water_icon.png"><br>
            Sewage water transportation
        </div>
        </a>
        
    </div>
</ul>
<!--ends banner-->
<!--About Section-->
<div id="about" class="container position" style="padding-bottom:0px;padding-top:0px;">
    <div class="row">
      <div class="col-lg-12 about_head">
      	<h1>About Us</h1>
      	<p>Established in 2008 Ayon International is today one of the Middle East's popular and more respected company. Our fleet offers an unparalleled range of services to our customers.
The company's line of business includes the distribution of water, waste water management, rental services of luxurious cars and lot more.
We collaborate with our clients to  understand their specific needs in details. We are committed to deliver high quality of plants, improved technology  with devoted services and our un-beaten competitiveness ensures complete customer satisfaction.</p>
<hr style="width:320px;border-color:#063"/>
      </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <img src="images/about.jpg">
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 about_right">
      <h2>Limousine Services and Rental Cars</h2><br>
      <p>We guarantee absolute sublime, reliability and professionalism providing an exemplary standard of car rental service to companies and private individuals. Our aim is to achieve the highest possible standard...</p>
      <a href="service.php">View More</a>
    </div>
  </div>
</div>
<!--servive Section-->
<div class="container-fluid" id="servive">
  <div class="container">
      <div class="row">
          <div class="col-lg-3 services_head">
            <h1 style="color:#fff">Our services</h1>
            <p style="margin-bottom:0; text-indent:20px;    word-spacing: -0.94px;">Ayon internationals is dedicated to offering quality service with the speed, reliability and value you deserve.</p><br><p style="margin:0; text-indent:20px;       word-spacing: -3.58px;"> We focus on consistent customer service and standardization of business practices which lead to customer delight. </p><br>
            <p style="margin-top:0; text-indent:20px;    word-spacing: -3px;">We provide a different services to customers like car rental services, waste water management, cleaning and maintenance etc.</p>
            <a href="service.php">View All</a>
          </div>
          <div class="col-lg-9" style="margin:30px auto 0px auto;">	
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 service_item">
              <img src="images/1.jpg">
              <h4>Supply of drinking water</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 service_item">
              <img src="images/2.jpg">
              <h4>Skip waste removal services</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 service_item">
              <img src="images/3.jpg">
              <h4>Cleaning service & Maintenance</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 service_item">
              <img src="images/4.jpg">
              <h4>Limousine services and Rental Cars</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 service_item">
              <img src="images/5.jpg">
              <h4>Tyre & Battery</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 service_item">
              <img src="images/6.jpg">
              <h4>Sewage water transportation</h4>
            </div>
         </div>
      </div>
  </div>
</div>
<div class="container-fluid mngmnt_msg_main">
	<div class="container">
    	<div class="row">
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mngmnt_msg">
               <h1>Management's Message</h1>
               <!--<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">
               		<img src="images/md.jpg">
               		<p style="text-align:center;color:#000;">Managing Director</p>
               </div>-->
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               		<p>The last few years have brought both challenge and change for SOLB MISR. We adopted a corporate structure that allows us to be a more competitive business and market player. The ongoing economic and social changes have required the focused attention of all our employees to improve performance and position us for success no matter what kind of markets we encounter. We’ve made excellent progress, but we have more to do. We will continue to take decisive action to strengthen and grow our group in a sustainable way. We firmly seek to strike the right balance between profits and principles. We will take courageous stands and we will continue to be active on behalf of issues such as environmental sustainability, equality and corporate social responsibilities.</p>
                     <p style="text-align:center;color:#000; text-align:right">Anas (Manager)</p>
               </div>
            </div>
        	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mngmnt_msg">
               <a href="gallery.php"><h1>Photo Gallery</h1></a>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               		<img src="images/gallery1.jpg">
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               		<img src="images/gallery2.jpg">
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               		<img src="images/gallery3.jpg">
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               		<img src="images/gallery4.jpg">
               </div>
            </div>            
        </div>
    </div>
</div>
<div class="container-fluid dwnld_brchr">
	<div class="container">
    	<div class="row">
        	<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <p>Want to know more about us?<br><span>Please go through our e-brochure.</span></p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-top:55px">
                <a href="download/company_profile.pdf" target="_blank">Download E-Brochure</a>  
            </div>
        </div>
    </div>
</div>
<div class="client">
	<h1>Our Clients</h1>
	<div id="amazingcarousel-container-1">
        <div id="amazingcarousel-1" style="display:none;position:relative;width:100%;max-width:1200px;margin:0px auto 0px;">
            <div class="amazingcarousel-list-container">
                <ul class="amazingcarousel-list">
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client1.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client2.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client3.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client4.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client5.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client7.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client8.jpg" /></div>
                        </div>
					</li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client9.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client10.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client11.jpg" /></div>
                        </div>
                    </li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client12.jpg" /></div>
                        </div>
					</li>
                    <li class="amazingcarousel-item">
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client13.jpg" /></div>
                        </div>
                    </li> 
                    <li class="amazingcarousel-item">  
                        <div class="amazingcarousel-item-container">
                            <div class="amazingcarousel-image"><img src="images/client14.jpg" /></div>
                        </div>  
                    </li>               
                </ul>
            </div>
            <div class="amazingcarousel-nav"></div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
        	<iframe style="width:100%; height:280px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3609.224554796155!2d51.43834781501016!3d25.229360583881167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjXCsDEzJzQ1LjciTiA1McKwMjYnMjUuOSJF!5e0!3m2!1sen!2s!4v1478261847966" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</div>        
<?php include('includes/footer.php'); ?>