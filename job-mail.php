<?php
header( "refresh:5;url=career.php" );
?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<style>
        body{
            background-color: #f5f7fa;
        }
        .panel-heading h4{
            margin: 0px;
        }
        .panel{
            max-width: 290px;
            margin: 50px auto 0px auto;
        }
	</style>
</head>
<body>
<?php

require_once('PHPMailer/class.phpmailer.php');


if($_POST) 
{
	
	$name		=	trim(ucfirst(strtolower($_POST['name'])));
	$position	=	trim(ucfirst(strtolower($_POST['position'])));
	$messege	=	trim(ucfirst(strtolower($_POST['messege'])));
	$phone		=	trim($_POST['phone']);
	$email		=	trim($_POST['mail']);
	
	$file		=	$_FILES["resume"]["tmp_name"];
	$filename	=	$_FILES["resume"]["name"];

	$emailTo = 'info@ayoninternational.com';
	$subject= "Mail from ayoninternational.com Career page ";
	$bodytext = " Name : ".$name."\r\n Position : ".$position."\r\n Phone : ".$phone."\r\n Email : ".$email."\r\n Messege: ".$messege."\r\n";
	 
	$email = new PHPMailer();
	$email->From      = "career@ayon.bodhiinfo.com";
	$email->FromName  = $name;
	$email->Subject   = $subject;
	$email->Body      = $bodytext;
	$email->AddAddress($emailTo);

	$file_to_attach = $file;

	$email->AddAttachment( $file_to_attach , $filename );


	if($email->Send())
	{ 
        echo '<div class="panel panel-primary"><div class="panel-heading"><h4>Success</h4></div><div class="panel-body">Message has been sent.You will be directed back soon.</div></div>';
	}
	else
	{
       
		echo '<div class="panel panel-danger"><div class="panel-heading"><h4 class="text-danger">Success</h4></div><div class="panel-body">Message could not be sent. Please try again. You will be directed back soon.</div></div>';
	}
}

?>
</body>
</html>